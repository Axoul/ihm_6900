module app {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.fazecast.jSerialComm;
    opens app to javafx.fxml;
    exports app;
}