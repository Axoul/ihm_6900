package app;

public class Parser {
    private static final byte SoF = (byte) 0xa0;
    private static final byte TX_TYPE_STATE_PCK = (byte) 0x01;
    private static final byte TX_TYPE_DESCRIPTOR_PCK = (byte) 0x02;
    private static final int BYTES_PER_SIZE = 6;
    private boolean ready;
    private byte lastID;
    private int size;

    private Controller mainController;

    public Parser(Controller mainController) {
        this.ready = false;
        this.lastID = (byte) 0xff;
        this.mainController = mainController;
    }

    /**
     * Parse la trame UART de type commande
     * @param inData
     */
    public void parseData(byte[] inData) {
        if ((inData[0] == SoF) && (!this.ready)) {
            switch (inData[2]) {
                case TX_TYPE_STATE_PCK:
                    //this.parseState();
                    break;

                case TX_TYPE_DESCRIPTOR_PCK:
                    this.size = this.byteToInt(new byte[] {inData[3], inData[4]});
                    this.ready = true;
                    break;
            }
        }
        else if (ready) {
            mainController.clearLidarData();
            this.parseLidar(inData);
            this.ready = false;
        }
    }

    private void parseState(byte[] stateData) {

    }

    /**
     * Parse les données du lidar après réception de la commande
     * @param lidarData
     */
    private void parseLidar(byte[] lidarData) {
        mainController.setIncoming("Lidar Points : " + (lidarData.length-3) + '\n');
        for (int i = 0; i < lidarData.length - 3 ; i+= BYTES_PER_SIZE) {
            if(i + 5 >= lidarData.length - 3) {
                break;
            }
            int distance_mm = this.byteToInt(new byte[] {lidarData[i], lidarData[i+1]});
            float angle = this.byteToFloat(new byte[] {lidarData[i+2], lidarData[i+3], lidarData[i+4], lidarData[i+5]});
            mainController.addLidarChart(mainController.polarToCartesian(distance_mm, angle));
        }
    }

    /**
     * Convertit 2 octets en entier de type int
     * @param inBytes
     * @return
     */
    private int byteToInt(byte[] inBytes) {
        return (inBytes[0] & 0xff) | ((inBytes[1] & 0xff) << 8);
    }

    /**
     * Convertit 4 octets en type flottant
     * @param inBytes
     * @return
     */
    private float byteToFloat(byte[] inBytes) {
        int asInt = (inBytes[0] & 0xff) | ((inBytes[1] & 0xff) << 8) | ((inBytes[2] & 0xff) << 16) | ((inBytes[3] & 0xff) << 24);
        return Float.intBitsToFloat(asInt);
    }

}
