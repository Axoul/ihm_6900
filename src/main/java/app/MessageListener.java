package app;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortMessageListener;
import javafx.application.Platform;

public class MessageListener implements SerialPortMessageListener {

    private Controller mainController;
    private Parser mainParser;

    public MessageListener(Controller mainController) {
        this.mainController = mainController;
        mainParser = new Parser(this.mainController);
    }

    @Override
    public byte[] getMessageDelimiter() {
        return new byte[] { (byte) '\r', (byte)'\r', (byte)'\n'};
    }

    @Override
    public boolean delimiterIndicatesEndOfMessage() {
        return true;
    }

    @Override
    public int getListeningEvents() {
        return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
    }

    @Override
    public void serialEvent(SerialPortEvent serialPortEvent) {
        byte[] rxMessage = serialPortEvent.getReceivedData();
        Platform.runLater(() -> {
            byte[] clnByte = rxMessage.clone();
            mainParser.parseData(clnByte);
        });
    }
}
