package app;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.*;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ResourceBundle;

import com.fazecast.jSerialComm.*;

import static com.fazecast.jSerialComm.SerialPort.FLOW_CONTROL_CTS_ENABLED;
import static com.fazecast.jSerialComm.SerialPort.FLOW_CONTROL_RTS_ENABLED;

public class Controller implements Initializable {

    private MessageListener listener;
    private SerialPort nowSerial;
    private SerialPort[] portSerial;
    private ObservableList<String> portNames;
    @FXML
    private ComboBox comport, combaudrate;
    @FXML
    private Button connectBtn, refreshBtn, closeBtn, startRobotBtn, startMotBtn, stopMotBtn,startLidarBtn , brosseBtn, uvBtn, pschitBtn;
    @FXML
    private ToggleButton history;
    @FXML
    private Slider rangeSlider;
    @FXML
    private TextArea textLog, incLog;
    @FXML
    private ScatterChart<Number, Number> chartLidar;
    @FXML
    private NumberAxis xaxe, yaxe;

    private XYChart.Series<Number, Number> zero, series;

    private XYChart.Data<Number, Number> lidarCenter;

    private byte[] sendBytes;

    private static int maxPoint = 10000;
    private boolean enaHistory = false;
    private static byte sofID = (byte) 0x01;
    private CRC8 crc;
    private byte toggleBrosse, toggleUV, togglePschit;

    private static final byte[] HEX_ARRAY = "0123456789ABCDEF".getBytes(StandardCharsets.US_ASCII);

    /**
     * Lancement automatique au démarrage du GUI
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.toggleBrosse = (byte) 0x00;
        this.toggleUV = (byte) 0x00;
        this.togglePschit = (byte) 0x00;
        this.getPort();
        this.addBaudrate();
        this.guiDesactive();

        zero = new XYChart.Series<>();
        series = new XYChart.Series<>();

        lidarCenter = new XYChart.Data<>(0.0, 0.0);
        zero.getData().add(lidarCenter);
        chartLidar.getData().addAll(series, zero);
        crc = new CRC8();

        rangeSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
                xaxe.setLowerBound(-newValue.doubleValue());
                xaxe.setUpperBound(newValue.doubleValue());
                yaxe.setLowerBound(-newValue.doubleValue());
                yaxe.setUpperBound(newValue.doubleValue());
            }
        });

    }

    /**
     * Ajoute la liste des ports USB-UART à comport
     */
    private void getPort() {
        portNames = FXCollections.observableArrayList();
        portSerial = SerialPort.getCommPorts();
        for (int i = 0; i < portSerial.length; i++) {
            portNames.add(portSerial[i].getSystemPortName());
        }
        comport.setItems(portNames);
    }

    /**
     * Liste les baudrate possible
     */
    private void addBaudrate() {
        combaudrate.getItems().addAll(
                4800,
                9600,
                19200,
                38400,
                57600,
                115200
        );
    }

    /**
     * Appel sur le bouton "Connect"
     */
    @FXML
    public void openConnection() {
        int index = comport.getSelectionModel().getSelectedIndex();
        int baudrate = Integer.parseInt(combaudrate.getSelectionModel().getSelectedItem().toString());
        nowSerial = portSerial[index];
        this.addToLog("Connection à " + nowSerial.toString() + " sur " + portNames.get(index));
        if (nowSerial.openPort()) {
            this.addToLog("Connection Reussie");
            this.guiActive();
            listener = new MessageListener(this);
            nowSerial.setBaudRate(baudrate);
            nowSerial.setFlowControl(FLOW_CONTROL_RTS_ENABLED | FLOW_CONTROL_CTS_ENABLED);
            nowSerial.addDataListener(listener);
        } else {
            this.addToLog("Echec de connection");
        }
    }

    /**
     * Appel sur le bouton "Close"
     */
    @FXML
    public void closeConnection() {
        nowSerial.removeDataListener();
        if (nowSerial.closePort()) {
            this.addToLog("Deconnection Reussie");
            this.guiDesactive();
            sofID = (byte) 0x01;
        } else {
            this.addToLog("Echec de déconnection");
        }
    }

    /**
     * Appel sur le bouton "Refresh"
     */
    @FXML
    public void refreshPort() {
        this.getPort();
    }

    /**
     * Ajout d'une String à la premiere zone de texte
     * @param logString
     */
    public void addToLog(String logString) {
        textLog.appendText(logString + "\n");
    }

    /**
     * Ajout d'une String à la seconde zone de texte
     * @param incoming
     */
    public void setIncoming(String incoming) {
        incLog.appendText(incoming);
    }

    /**
     * Désactive les boutons lors de la déconnexion
     */
    private void guiDesactive() {
        incLog.clear();

        closeBtn.setDisable(true);
        connectBtn.setDisable(false);
        refreshBtn.setDisable(false);

        startRobotBtn.setDisable(true);
        startMotBtn.setDisable(true);
        stopMotBtn.setDisable(true);
        startLidarBtn.setDisable(true);
        brosseBtn.setDisable(true);
        uvBtn.setDisable(true);
        pschitBtn.setDisable(true);
    }

    /**
     * Active les boutons lors de la connection
     */
    private void guiActive() {
        closeBtn.setDisable(false);
        connectBtn.setDisable(true);
        refreshBtn.setDisable(true);

        startRobotBtn.setDisable(false);
        startMotBtn.setDisable(false);
        stopMotBtn.setDisable(false);
        startLidarBtn.setDisable(false);
        brosseBtn.setDisable(false);
        uvBtn.setDisable(false);
        pschitBtn.setDisable(false);
    }

    /**
     * Appel sur le bouton "Clear Graph"
     */
    @FXML
    public void removeLidarData() {
        clearLidarData();
    }

    /**
     * Appel sur le bouton "Start Robot"
     */
    @FXML
    public void sendStartRobot() {
        sendCustomBytes((byte) 0xa1, (byte) 0x00);
    }

    /**
     * Appel sur le bouton "Start Motor"
     */
    @FXML
    public void sendStartMot() {
        sendCustomBytes((byte) 0xa2, (byte) 0x00);
    }

    /**
     * Appel sur le bouton "Stop Motor"
     */
    @FXML
    public void sendStopMot() {
        sendCustomBytes((byte) 0xa3, (byte) 0x00);
    }

    /**
     * Appel sur le bouton "Start Lidar"
     */
    @FXML
    public void sendStartLidar() {
        sendCustomBytes((byte) 0xa4, (byte) 0x00);
    }

    /**
     * Appel sur le bouton "Brosse"
     */
    @FXML
    public void sendBrosse() {
        this.toggleBrosse ^= 1;
        sendCustomBytes((byte) 0xa5, toggleBrosse);
    }

    /**
     * Appel sur le bouton "UV"
     */
    @FXML
    public void sendUV() {
        this.toggleUV ^= 1;
        sendCustomBytes((byte) 0xa7, toggleUV);
    }

    /**
     * Appel sur le bouton "Pschit"
     */
    @FXML
    public void sendPschit() {
        this.togglePschit ^= 1;
        sendCustomBytes((byte) 0xa6, togglePschit);
    }

    /**
     * Création d'une trame UART de type commande
     * @param idFrame
     * @param data
     */
    private void sendCustomBytes(byte idFrame, byte data) {
        sendBytes = new byte[5];
        sendBytes[0] = (byte) 0xa0;
        sendBytes[1] = sofID;
        sendBytes[2] = idFrame;
        sendBytes[3] = data;
        crc.update(new byte[]{sendBytes[2], sendBytes[3]},0,2);
        sendBytes[4] = crc.getValue();
        crc.reset();
        nowSerial.writeBytes(sendBytes, sendBytes.length);
        this.incSof();
        this.setIncoming(bytesToHex(sendBytes)+'\n');
    }

    /**
     * Ajout d'un point au graphique Lidar
     * @param inData
     */
    public void addLidarChart(XYChart.Data<Number, Number> inData) {
        series.getData().add(inData);
        if (history.isSelected() && series.getData().size() > maxPoint) {
            series.getData().remove(0);
        }
    }

    /**
     * Efface les points du graphique Lidar
     */
    public void clearLidarData() {
        series.getData().clear();
    }

    /**
     * Convertit les point polaire en catésien et les prépare à l'ajout sur graphique
     * @param distance
     * @param angle
     * @return
     */
    public XYChart.Data<Number, Number> polarToCartesian(int distance, float angle) {
        float r = ((float) distance) / 500;
        float teta = (float) Math.toRadians(angle);
        return new XYChart.Data<>((float) (r * Math.cos(teta)), (float) (r * Math.sin(teta)));
    }

    /**
     * Incrémente l'id des trame de type commande
     */
    private void incSof() {
        sofID++;
    }

    /**
     * Affiche en String les valeurs hexadécimales d'un octet
     * @param bytes
     * @return
     */
    public static String bytesToHex(byte[] bytes) {
        byte[] hexChars = new byte[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars, StandardCharsets.UTF_8);
    }

}
